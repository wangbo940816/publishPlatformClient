<h1 align="center">Welcome to publishplatformclient 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://github.com/BoWang816/publishPlatformClient#readme" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://github.com/BoWang816/publishPlatformClient/graphs/commit-activity" target="_blank">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" />
  </a>
  <a href="https://github.com/BoWang816/publishPlatformClient/blob/master/LICENSE" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/github/license/BoWang816/publishplatformclient" />
  </a>
</p>

> 基于react与mobx的多平台发布平台

### 🏠 [Homepage](https://github.com/BoWang816/publishPlatformClient#readme)

### ✨ [Demo](https://publish.wangboweb.site)

## Install

```sh
npm install
```

## Run tests

```sh
npm run test
```

## Author

👤 **bo.wang**

* Website: https://blog.wangboweb.site
* Github: [@BoWang816](https://github.com/BoWang816)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/BoWang816/publishPlatformClient/issues). You can also take a look at the [contributing guide](https://github.com/BoWang816/publishPlatformClient/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2021 [bo.wang](https://github.com/BoWang816).<br />
This project is [MIT](https://github.com/BoWang816/publishPlatformClient/blob/master/LICENSE) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_